package com.mindhub.homebanking;

import com.mindhub.homebanking.models.*;
import com.mindhub.homebanking.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import java.time.LocalDateTime;
import java.util.Arrays;

@SpringBootApplication
public class HomebankingApplication {
	@Autowired
	private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(HomebankingApplication.class, args);
		System.out.println("Hola mundo");
	}
	@Bean
	public CommandLineRunner initData(ClientRepository repositoryClient,
									  AccountRepository repositoryAccount,
									  TransactionRepository repositoryTransaction,
									  LoanRepository repositoryLoan,
									  ClientLoanRepository repClientL,
									  CardRepository repCard
	) {
		return (args) -> {
			// save a couple of customers
			Client admin = repositoryClient.save(new Client("ADMIN", "ADMIN", "admin@mindhub.com", passwordEncoder.encode("admin")));
			Client clientOne = repositoryClient.save(new Client("Melba", "Morel", "melba@mindhub.com",passwordEncoder.encode("Admin1234")));
			Client clientTwo =repositoryClient.save(new Client("Hernesto", "Roblez", "hernesto.roblez@mindhub.com",passwordEncoder.encode("hernestokk")));
			Client clientThree =repositoryClient.save(new Client("Matias", "Lopez", "matilopez1999@mindhub.com", passwordEncoder.encode("matiaskpo")));
			Client clientFour =repositoryClient.save(new Client("Micaela", "Astrada", "micaastrada82@mindhub.com", passwordEncoder.encode("mica8234")));
			Account accountOne = repositoryAccount.save(new Account("VIN001", LocalDateTime.now(),5000, AccountType.cuentaCorriente, clientOne));
			Account accountTwo = repositoryAccount.save(new Account("VIN002", LocalDateTime.now().plusDays(1) ,7500, AccountType.cajaDeAhorroEnPesos , clientTwo));
			Account accountThree = repositoryAccount.save(new Account("VIN003", LocalDateTime.now().plusDays(2) ,10500, AccountType.cajaDeAhorroEnDolares, clientThree));


			Loan hipotecario = repositoryLoan.save(new Loan("Hipotecario", 500000F, Arrays.asList(12,24,36,48,60), 0.50));
			Loan personal = repositoryLoan.save(new Loan("Personal", 100000F, Arrays.asList(6,12,24), 0.60));
			Loan automotriz = repositoryLoan.save(new Loan("Automotriz", 300000F, Arrays.asList(6,12,24,36), 0.40));

			repClientL.save(new ClientLoan(300000F, 24, clientOne, hipotecario));
			repClientL.save(new ClientLoan(500000F, 12, clientOne, personal));
			repClientL.save(new ClientLoan(120000F, 24, clientTwo, personal));
			repClientL.save(new ClientLoan(250000F, 36, clientTwo, automotriz));

			repCard.save(new Card(CardType.DEBIT, CardColor.GOLD, clientOne));
			repCard.save(new Card(CardType.CREDIT, CardColor.TITANIUM, clientOne));
			repCard.save(new Card(CardType.CREDIT, CardColor.SILVER, clientTwo));

		};
	}

}
