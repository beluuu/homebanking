package com.mindhub.homebanking.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class Empleado {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "native")
    @GenericGenerator(name="native",strategy = "native")
    private Long id;

    @ManyToOne(fetch =  FetchType.EAGER)
    @JoinColumn(name = "sucursal_id")
    private Sucursal sucursal;

    private String nombre;
    private String apellido;
    private int edad;
    private int antiguedad;
    private int dni;
    private String cargo;
    private int sueldo;


    public Empleado() {
    }

    public Empleado(String nombre, String apellido, int edad, int antiguedad, int dni, String cargo, int sueldo, Sucursal sucursal) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.antiguedad = antiguedad;
        this.dni = dni;
        this.cargo = cargo;
        this.sueldo = sueldo;
        this.sucursal = sucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public int getAntiguedad() {
        return antiguedad;
    }

    public void setAntiguedad(int antiguedad) {
        this.antiguedad = antiguedad;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public int getSueldo() {
        return sueldo;
    }

    public void setSueldo(int sueldo) {
        this.sueldo = sueldo;
    }

    public Sucursal getNumeroDeSucursal() {
        return sucursal;
    }

    public void setNumeroDeSucursal(Sucursal numeroDeSucursal) {
        this.sucursal = numeroDeSucursal;
    }

    @Override
    public String toString() {
        return "Empleado{" +
                "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", edad=" + edad +
                ", antiguedad=" + antiguedad +
                ", dni=" + dni +
                ", cargo='" + cargo + '\'' +
                ", sueldo=" + sueldo + '\'' +
                ", sucursal=" +sucursal +
                '}';
    }
}
