package com.mindhub.homebanking.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Sucursal {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO,generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;

    @OneToMany(mappedBy = "sucursal",fetch = FetchType.EAGER)
    Set<Empleado> empleados = new HashSet<>();

    private int cantidadEmpleados;
    private int numeroDeSucursal;
    private int cantidadCajerosAutomaticos;

    public Sucursal() {
    }

    public Sucursal(int cantidadEmpleados, int numeroDeSucursal, int cantidadCajerosAutomaticos) {
        this.cantidadEmpleados = cantidadEmpleados;
        this.numeroDeSucursal = numeroDeSucursal;
        this.cantidadCajerosAutomaticos = cantidadCajerosAutomaticos;
    }

    public int getCantidadEmpleados() {
        return cantidadEmpleados;
    }

    public void setCantidadEmpleados(int cantidadEmpleados) {
        this.cantidadEmpleados = cantidadEmpleados;
    }

    public int getNumeroDeSucursal() {
        return numeroDeSucursal;
    }

    public void setNumeroDeSucursal(int numeroDeSucursal) {
        this.numeroDeSucursal = numeroDeSucursal;
    }

    public int getCantidadCajerosAutomaticos() {
        return cantidadCajerosAutomaticos;
    }

    public void setCantidadCajerosAutomaticos(int cantidadCajerosAutomaticos) {
        this.cantidadCajerosAutomaticos = cantidadCajerosAutomaticos;
    }
}
