

let btn = document.querySelector("#btn");
   let sidebar = document.querySelector(".sidebar");
   let searchBtn = document.querySelector(".bx-search");

   btn.onclick = function() {
     sidebar.classList.toggle("active");
   }
   searchBtn.onclick = function() {
     sidebar.classList.toggle("active");
   }


//Se define el ID  del cliente 
function definirCliente(){
    let url = window.location.search;
    if (url != ""){
        let parametros = definirParametros(url);
        let cliente = parametros[0];
        return cliente;
        

    }else{
        let nueva = "Sin cliente seleccionado";
        console.log(nueva)
        return nueva;
    }

};
//DEFINE LA CUENTA
function definirSegundo(){
    let url = window.location.search;
    let parametros = definirParametros(url);
    if (parametros[1] == null){
        let segundo = parametros[1];
        
        return segundo;
        

    }else{
        let nueva = "Seccion Cuentas";
        console.log(nueva)
        return nueva;
    }

};

function definirParametros(url){
    let nuevo = url.split("&");
    let result = [];
    nuevo.forEach(x => {
        let separator = x.split("=");
        separator.shift();
        x = separator;
        result.push(x);

        
    })
    
    return result;
}

/*MENU */

/*MENU USER*/
function generateUser(){
    let urlUser = "client.html?id=" + definirCliente();
    let user = document.getElementById("users");
    user.href =urlUser;
    console.log(urlUser);
}


/*MENU ACCOUNTS*/
function generateAccounts(){
    let urlAccount ="accounts.html?id=" + definirCliente();
    let accounts = document.getElementsByClassName("accounts");
    console.log(accounts)
    let i;
    for(i = 0; i < accounts.length; i++ ){
        accounts[i].href = urlAccount;
        console.log(accounts[i]);
    }
}



/*MENU LOANS*/
function generateLoans(){
    let urlLoans = "loans.html?id=" + definirCliente();
    let loans = document.getElementsByClassName("loans");
    let i;
    for(i = 0; i < loans.length; i++ ){
        loans[i].href = urlLoans;
        console.log(loans[i]);
    }
}

/*MENU CARDS*/
function generateCards(){
    let urlCards = "Cards.html?id=" + definirCliente();
    let cards = document.getElementsByClassName("cards");
    let i;
    for(i = 0; i < cards.length; i++ ){
        cards[i].href = urlCards;
        console.log(cards[i]);
    }
}

//CAMBIAR DATE
function imprimirFecha(date){
           let newDato = new Date(date);
           let month = newDato.getMonth();
           let day = newDato.getDay();
           let year = newDato.getFullYear();
           let fecha = day + "-"+ month + "-" + year; 
           return fecha;
}





const app = Vue.createApp({
    data(){
        return{
        clientes: [],
        accounts: [],
        name : "",
        lastname : "",  
        cliente: [],
        dni: "",
        email: "",
        numeroDeCuenta: "",
        saldo: 0,
        

        }
    },
    methods:{
      

    },
    computed:{
        
    },

    created(){
        axios.get('http://localhost:8080/api/clientes')
        .then(resp => {
            
            this.clientes = resp.data;
            console.log(this.clientes);
            this.id = definirCliente();
            if(this.id !== "Sin cliente seleccionado"){
                    let client = this.clientes[parseInt(this.id) - 1];
                    this.cliente.push(client);
                    generateUser();
                    generateAccounts();
                    generateLoans();
                    generateCards();
                    this.name = this.cliente[0].nombre;
                    this.lastname = this.cliente[0].apellido;
                    this.email = this.cliente[0].email;
                    this.dni = this.cliente[0].dni;
                    this.accounts = this.cliente[0].cuentas;
                    this.accounts.forEach(x=>{
                         var fecha = imprimirFecha(x.fechaDeCreacion);
                         x.fechaDeCreacion = fecha;
                         
                    })
                    
            }else{
                console.log("Sin cliente definido")
            }
            
            
        })
        .catch(err => console.log(err));

       
}
}).mount('#app');