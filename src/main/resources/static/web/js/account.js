let btn = document.querySelector("#btn");
   let sidebar = document.querySelector(".sidebar");
   let searchBtn = document.querySelector(".bx-search");

   btn.onclick = function() {
     sidebar.classList.toggle("active");
   }
   searchBtn.onclick = function() {
     sidebar.classList.toggle("active");
   }

function definirCuenta(urlId){
    if (urlId != ""){
        
        let cuenta = definir(urlId);
        let cuentaTotal = cuenta[0];
        return cuentaTotal;
        

    }else{
        let nueva = "Seccion Cuentas";
        console.log(nueva)
        return nueva;
    }

};
function definir(urlId){
    let nuevo = urlId.split("=");
    nuevo.shift();
    return nuevo;
}

const app = Vue.createApp({
    data(){
        return{
        clientes: [],
        account: [],
        accounts: [],
        accountActive: 0,
        accountNumber: 0,
        balance: 0,
        idAccount: "",
        name : "",
        lastname : "",
        dateCuenta: "",
        yearAccount: "",
        cliente: [],
        loans: [],
        lastLoans: [],
        dni: "",
        email: "",
        transactions:[], 
        ammount:0,
        urlId: "",
        LoansBool: false,
        
        }
    },
    methods:{
        setLoansBool(){
            this.LoansBool = !this.LoansBool;
        }

    },

    created(){
        axios.get('http://localhost:8080/api/clientes')
        .then(resp => {
            
            this.clientes = resp.data;
            console.log(this.clientes);
            let url = window.location.search;
            this.urlId = url;
            this.id = definirCuenta(this.urlId);
            let client = this.clientes[parseInt(this.id) - 1];
            this.cliente.push(client);
            console.log(this.cliente);
            this.accounts = this.cliente[0].cuentas;
            this.accountActive = this.clientes[parseInt(this.id)-1].cuentas;
            this.account = this.clientes[parseInt(this.id)-1].cuentas[0];
            this.accountNumber = this.account.numeroDeCuenta;
            
            this.ammount = this.account.saldo;
            this.idAccount = this.account.id;
            let dato = this.account.fechaDeCreacion;
            this.dateCuenta = new Date(dato);
            console.log(this.dateCuenta);
            
            this.transactions = this.account.trasactions;
            let array = this.transactions;
            array.forEach(element => {
                let datetemporal = new Date(element.date);
                element.date = datetemporal;
            
            });

            this.transactions = array;
            this.name = this.cliente[0].nombre;
            this.lastname = this.cliente[0].apellido;
            this.loans = this.cliente[0].loans;
            this.lastLoans = this.loans[(this.loans.length) -1];
            this.dni = this.cliente[0].dni;


           
            
        })
        .catch(err => console.log(err));

        /*
        axios.get('/api/clientes/1')
        .then(resp => {
            this.clientes= resp.data; 
            console.log(this.clientes.nombre)
            this.nombre = this.clientes.nombre;
            console.log(this.nombre);
            this.apellido = this.clientes.apellido;
            this.dni = this.clientes.dni;
            this.email = this.clientes.email;
            this.cuentas = this.clientes.cuentas;  
        })
        .catch(err => console.log(err));
         setTimeout(function(){
            console.log("hola,este mjs tardo 1 seg");
            
        },2000);
    }*/
}
}).mount('#app');