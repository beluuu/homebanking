

const app = Vue.createApp({
    data(){
        return{
        clientes: [],
        firstName : null,
        lastName : null,  
        client: [],
        email: null,
        password:null,
        login401: false,
        emailError: false,
        datosError: false,
        load:false,
        }
    },
    methods:{
        vaciar(){
            this.firstName = null;
            this.lastName = null;
            this.email = null;
            this.password = null;
            
        },
        iniciar(){
            let request = "email="+this.email+"&password="+this.password;
            console.log(request);
            axios.post('/api/login',request,
            {headers:{'content-type':'application/x-www-form-urlencoded'}})
            .then(response => {
                if(this.login401){this.login401 = false;}
                console.log('signed in!!!');
                window.location.href = "/web/html/client.html";
            })
            .catch(error => {
                console.log(error)
                let state = error.response.status;
                if(state == 401){this.login401 = true;}
            }); 
        },

        loguear(){
            if(this.firstName != null && this.lastName != null && this.email != null && this.password != null){
                if(this.email.includes('@'&& '.')){
                    let request = "firstName="+ this.firstName+"&lastName="+this.lastName+"&email="+this.email+"&password="+ this.password;
                    axios.post('/api/clients',request,
                    {headers:{ 'content-type':'application/x-www-form-urlencoded'}})
                    .then(response => {
                        
                        axios.post('/api/login',request,
                        {headers:{'content-type':'application/x-www-form-urlencoded'}})
                        setTimeout(function(){
                            window.location.href= "/web/html/client.html";
                        },2000) 
                        });
                }else{
                    if(this.datosError){this.datosError = false}
                    this.emailError = true;
                    console.log("error email")
                    this.vaciar();
                }
            }else{
                if(this.emailError){this.emailError = false}
                this.datosError = true;
                console.log("datos mal");
                this.vaciar();
            }
                
        },
        
            
        
    },

    created(){
       this.load = false;
       
       
    }
}).mount('#app');