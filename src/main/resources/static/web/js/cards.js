
/*MENU*/
let btn = document.querySelector("#btn");
let sidebar = document.querySelector(".sidebar");
let searchBtn = document.querySelector(".bx-search");

btn.onclick = function() {
  sidebar.classList.toggle("active");
}
searchBtn.onclick = function() {
  sidebar.classList.toggle("active");
}
/*BUTTON*/
   const barOuter = document.querySelector(".bar-outer");
   const options = document.querySelectorAll(".bar-grey .option");
   let current = 1;
   options.forEach((option, i) => (option.index = i + 1));
   options.forEach(option =>
    option.addEventListener("click", function() {
     barOuter.className = "bar-outer";
     barOuter.classList.add(`pos${option.index}`);
     if (option.index > current) {
       barOuter.classList.add("right");
     } else if (option.index < current) {
       barOuter.classList.add("left");
     }
     current = option.index;
   }));

   const barOuter2 = document.querySelector(".bar-outer2");
   const options2 = document.querySelectorAll(".bar-grey2 .option2");
   let current2 = 1;
   options2.forEach((option, i) => (option.index = i + 1));
   options2.forEach(option =>
                   option.addEventListener("click", function() {
     barOuter2.className = "bar-outer2";
     barOuter2.classList.add(`pos${option.index}`);
     if (option.index > current2) {
       barOuter2.classList.add("right");
     } else if (option.index < current2) {
       barOuter2.classList.add("left");
     }
     current2 = option.index;
   }));


//Se define el ID  del cliente 
function definirCliente(){
 let url = window.location.search;
 if (url != ""){
     let parametros = definirParametros(url);
     let cliente = parametros[0];
     return cliente;
     

 }else{
     let nueva = "Sin cliente seleccionado";
     console.log(nueva)
     return nueva;
 }

};
//DEFINE LA CUENTA
function definirSegundo(){
 let url = window.location.search;
 let parametros = definirParametros(url);
 if (parametros[1] == null){
     let segundo = parametros[1];
     
     return segundo;
     

 }else{
     let nueva = "Seccion Cuentas";
     console.log(nueva)
     return nueva;
 }

};

function definirParametros(url){
 let nuevo = url.split("&");
 let result = [];
 nuevo.forEach(x => {
     let separator = x.split("=");
     separator.shift();
     x = separator;
     result.push(x);

     
 })
 
 return result;
}

/*MENU */

/*MENU USER*/
function generateUser(){
 let urlUser = "client.html?id=" + definirCliente();
 let user = document.getElementById("user");
 user.href =urlUser;
 console.log(urlUser);
}


/*MENU ACCOUNTS*/
function generateAccounts(){
 let urlAccount ="accounts.html?id=" + definirCliente();
 let accounts = document.getElementsByClassName("accounts");
 console.log(accounts)
 let i;
 for(i = 0; i < accounts.length; i++ ){
     accounts[i].href = urlAccount;
     console.log(accounts[i]);
 }
}



/*MENU LOANS*/
function generateLoans(){
 let urlLoans = "loans.html?id=" + definirCliente();
 let loans = document.getElementsByClassName("loans");
 let i;
 for(i = 0; i < loans.length; i++ ){
     loans[i].href = urlLoans;
     console.log(loans[i]);
 }
}

/*MENU CARDS*/
function generateCards(){
 let urlCards = "Cards.html?id=" + definirCliente();
 let cards = document.getElementsByClassName("cards");
 let i;
 for(i = 0; i < cards.length; i++ ){
     cards[i].href = urlCards;
     console.log(cards[i]);
 }
}


//CAMBIAR DATE
function imprimirFecha(date){
        let newDato = new Date(date);
        let month = newDato.getMonth();
        let day = newDato.getDay();
        let year = newDato.getFullYear();
        let fecha = day + "-"+ month + "-" + year; 
        return fecha;
}




const app = Vue.createApp({
 data(){
     return{
     clientes: [],
     accounts: [],
     name : "",
     lastname : "",  
     cliente: [],
     dni: "",
     email: "",
     numeroDeCuenta: "",
     saldo: 0,
     cards: [],
     credito: false,
     debito: false,
     titanium: false,
     gold: false,
     silver: false,
     credTitanium: true,
     credGold: false,
     credSilver: false,
     debTitanium: false,
     debGold: false,
     debSilver: false,
     sinCard: false,
     }
 },
 methods:{

  
    cred() {
      this.credito = true;
      actualizar(this.cards);
    },
    deb(){
      this.debito = true;
      actualizar(this.cards);
    },
    tita(){
      this.titanium = true;
      actualizar(this.cards);
    },
    gol(){
      this.gold = true;
      actualizar(this.cards);
    },
    silve(){
      this.silver = true;
      actualizar(this.cards);
    }
 },
 computed:{
    
  desactivarMenos(tipo,color){
    if(tipo == "CREDIT"){
        if(color == "TITANIUM"){
          this.credito= true;
          this.debito= false;
          this.titanium= true;
          this.gold= false;
          this.silver= false;
          this.credTitanium= true;
          this.credGold= false;
          this.credSilver= false;
          this.debTitanium= false;
          this.debGold= false;
          this.debSilver= false;
          this.sinCard = false;
        }
        else if(color =="GOLD"){
          this.credito= true;
          this.debito= false;
          this.titanium= false;
          this.gold= true;
          this.silver= false;
          this.credTitanium= false;
          this.credGold= true;
          this.credSilver= false;
          this.debTitanium= false;
          this.debGold= false;
          this.debSilver= false;
          this.sinCard = false;
        }else if( color == "SILVER"){
          this.credito= true;
          this.debito= false;
          this.titanium= false;
          this.gold= false;
          this.silver= true;
          this.credTitanium= false;
          this.credGold= false;
          this.credSilver= true;
          this.debTitanium= false;
          this.debGold= false;
          this.debSilver= false;
          this.sinCard = false;
        }
    }
    else if(tipo =="DEBIT"){
      if(color == "TITANIUM"){
        this.credito= false;
        this.debito= true;
        this.titanium= true;
        this.gold= false;
        this.silver= false;
        this.credTitanium= false;
        this.credGold= false;
        this.credSilver= false;
        this.debTitanium= true;
        this.debGold= false;
        this.debSilver= false;
        this.sinCard = false;
      }else if(color =="GOLD"){
        this.credito= false;
        this.debito= true;
        this.titanium= false;
        this.gold= true;
        this.silver= false;
        this.credTitanium= false;
        this.credGold= false;
        this.credSilver= false;
        this.debTitanium= false;
        this.debGold= true;
        this.debSilver= false;
        this.sinCard = false;
      }else if ( color == "SILVER"){
        this.credito= false;
        this.debito= true;
        this.titanium= false;
        this.gold= false;
        this.silver= true;
        this.credTitanium= false;
        this.credGold= false;
        this.credSilver= false;
        this.debTitanium= false;
        this.debGold= false;
        this.debSilver= true;
        this.sinCard = false;
      }
    }
  },
  actualizar(cards){
    cards.forEach(element =>{
      if(validar(element) == "EXITOSO"){
        desactivarMenos(element.tipo,element.color);
      }
     });
     if(nuevo.length == 0){
       this.sinCard = true;
     }
     
  },
  validar(element){
      if(this.credito && element.tipo == "CREDIT"){
        if(this.titanium && element.color == "TITANIUM"){

          return "EXITOSO";
        }else if (this.gold && element.color =="GOLD"){
          return "EXITOSO";
        }else if(this.silver && element.color== "SILVER"){
          return "EXITOSO";
        }
      }else if(this.debito && element.tipo == "DEBIT"){
        if(this.titanium && element.color == "TITANIUM"){
          return "EXITOSO";
        }else if (this.gold && element.color =="GOLD"){
          return "EXITOSO";
        }else if(this.silver && element.color== "SILVER"){
          return "EXITOSO";
        }
      }else{
        return "NO EXITOSO";
      }
    },
 },


 created(){
     axios.get('http://localhost:8080/api/clientes')
     .then(resp => {
         
         this.clientes = resp.data;
         console.log(this.clientes);
         this.id = definirCliente();
         if(this.id !== "Sin cliente seleccionado"){
                 let client = this.clientes[parseInt(this.id) - 1];
                 this.cliente.push(client);
                 generateUser();
                 generateAccounts();
                 generateLoans();
                 generateCards();
                 this.name = this.cliente[0].nombre;
                 this.lastname = this.cliente[0].apellido;
                 this.email = this.cliente[0].email;
                 this.dni = this.cliente[0].dni;
                 this.cards = this.cliente[0].cards;
                 
         }else{
             console.log("Sin cliente definido")
         }
         
         
     })
     .catch(err => console.log(err));

    
}
}).mount('#app');

