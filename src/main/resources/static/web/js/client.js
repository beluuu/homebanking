
let btn = document.querySelector("#btn");
   let sidebar = document.querySelector(".sidebar");
   let searchBtn = document.querySelector(".bx-search");

   btn.onclick = function() {
     sidebar.classList.toggle("active");
   }
   searchBtn.onclick = function() {
     sidebar.classList.toggle("active");
   }



const app = Vue.createApp({
    data(){
        return{
        client: [],
        firstName : "",
        lastName : "",  
        id: "",
        email: "",
        loans:[],
        accounts: [],
        cards: [],
        }
    },
    methods:{
        

    },

    created(){
        axios.get('/api/clients/current', { headers: { 'accept': 'application/xml' } })
        .catch(err => console.log(err))
        axios.get('/api/clients/current')
        .then(result =>{
            console.log(result.data)
            this.client = result.data;
            this.firstName = this.client.firstName;
            this.lastName = this.client.lastName;
            this.email = this.client.email;
            this.id = this.client.id;
            this.loans= this.client.loans;
            this.accounts =this.client.accounts;
            this.cards = this.client.cards;

        })
       
}
}).mount('#app');